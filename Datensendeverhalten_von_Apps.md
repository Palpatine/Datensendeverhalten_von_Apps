### Datensendeverhalten von Apps

|      Herausgeber      |      App      |  Downloads |  Version |  Datenschutzfreundlich | Persönlicher Test | Exodus   | Weitere Tests | Medienberichte |
|:-------------|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|:-------------:|
|Allianz|[Allianz Gesundheits-App](https://goc-eportale.allianz.de/dlc_app/Print/DlcPdfPrintService/APKV-0081Z0.pdf?posNr=null&appSessionId=dlc:AB6F6660E077A9BC7A504665C11E6A1C&userSessionId=fQ2mJ4Jn7JXFv34wK2rdRcD8ED12D72A4E647672&tokenId=IdvHRRiAX)|50,000+|/|/|/|/|/|/|
|AOK|[AOK Abnehmen mit Genuss](https://www.aok.de/pk/uni/inhalt/die-app-zu-abnehmen-mit-genuss/)|100,000+|/|/|/|/|/|/|
|AOK|[AOK Bonus-App](https://www.aok.de/pk/plus/inhalt/bonusprogramm/#c50692)|100,000+|/|/|/|/|/|/|
|AOK|[AOK Schwangere](https://www.aok.de/pk/uni/inhalt/aok-app-schwangerschaft/)|100,000+|/|/|/|/|/|/|
|AOK|[AOK Vorsorge](https://www.aok.de/pk/uni/inhalt/aok-app-vorsorge-auf-deutsch-und-tuerkisch/)|5,000+|/|/|/|/|/|/|
|AOK|[Mein AOK](https://www.aok.de/pk/uni/inhalt/app-zum-onlineportal-meine-aok-2/)|100,000+|/|/|/|/|/|/|
|Audi BKK|[StartStop-Rauchfrei](https://www.audibkk.de/leistungen/leistungen-von-a-z/r/rauchfrei-app-startstop/)|5,000+|/|/|/|/|/|/|
|Barmenia|[MediApp-Telearzt](https://www.barmenia.de/de/service/krankenversicherung-1/beratungsleistungen/mediapp_telemedizin.xhtml)|1,000+|/|/|/|/|/|/|
|Barmenia|[RechnungsApp](https://www.barmenia.de/de/barmenia/barmenia_apps_1/rechnungsapp_3/rechnungsapp.xhtml)|50,000+|/|/|/|/|/|/|
|BARMER|[Arzt- und Krankenhaussuche-App](https://www.barmer.de/gesundheitscampus/apps/arzt-und-krankenhaussuche)|5.000+|/|/|/|/|/|/|
|BARMER|[BAMER-App](https://www.barmer.de/gesundheitscampus/apps/service-app-9080)|500,000+|/|/|/|/|/|/|
|BARMER|[Bonus-App](https://www.barmer.de/gesundheitscampus/apps/bonus-app)|50.000+|/|/|/|/|/|/|
|BARMER|[Headache-hurts-App](https://www.barmer.de/gesundheitscampus/apps/headache-hurts-167896)|1.000+|/|/|/|/|/|/|
|BARMER|[Kaia Rücken-App](https://www.barmer.de/gesundheitscampus/apps/kaia-ruecken-app)|100.000+|/|/|/|/|/|/|
|BARMER|[Kindernotfall-App](https://www.barmer.de/gesundheitscampus/apps/kindernotfall-app-123954)|50.000+|/|/|/|/|/|/|
|BARMER|[Knie-App Kniekontrolle](https://www.barmer.de/gesundheitscampus/apps/knie-app-105678)|5.000+|/|/|/|/|/|/|
|BARMER|[Mimi Hörtest App](https://www.barmer.de/gesundheitscampus/apps/mimi-app)|10.000+|/|/|/|/|/|/|
|BARMER|[Schlafenszeit (Google Assistant)](https://assistant.google.com/services/a/uid/0000003913480391?hl=de)|?|/|/|/|/|/|/|
|BARMER|[Schlafenszeit (Amazon Alexa)](https://www.amazon.de/BARMER-Barmer-Schlafenszeit/dp/B078HZ1MPT)|?|/|/|/|/|/|/|
|BARMER|[StudySmarter App](https://www.barmer.de/gesundheitscampus/apps/studysmarter-app)|100.000+|/|/|/|/|/|/|
|BARMER|[Teledoktor-App](https://www.barmer.de/gesundheitscampus/apps/teledoktor-app-100648)|10.000+|/|/|/|/|/|/|
|BARMER|[7Mind: Meditations-App für Entspannung und Achtsamkeit](https://www.barmer.de/gesundheitscampus/apps/7mind)|500.000+|2.6.1|Nein|[Link](https://www.kuketz-blog.de/7mind-meditations-app-ohne-google-geht-nichts-anmeldung-via-facebook/)|[4](https://reports.exodus-privacy.eu.org/en/reports/99373/)|/|/|
|Central|[GesundheitsApp](https://www.central.de/app/)|100,000+|/|/|/|/|/|/|
|Daimler BKK|[Daimler BKK](https://www.daimler-bkk.com/service-kontakt/mediencenter/daimler-bkk-app/)|10,000+|/|/|/|/|/|/|
|DAK|[DAK App](https://www.central.de/app/)|50,000+|/|/|/|/|/|/|
|DAK|[DAK Scan-App](https://www.dak.de/dak/ihr-anliegen/dak-scan-app-2076820.html)|100,000+|/|/|/|/|/|/|
|DAK|[DAK Pflege-App](https://www.dak.de/dak/ihr-anliegen/dak-pflegeguide-2076130.html)|5,000+|/|/|/|/|/|/|
|Debeka|[LeistungsApp](https://www.debeka.de/service/app/leistungs-app/index.html)|100,000+|/|/|/|/|/|/|
|HEK|[HEK Service-App](https://www.hek.de/hek-digital/hek-service-app/)|50,000+|/|/|/|/|/|/|
|HKK|[Movival](https://www.hkk.de/leistungen-und-services/hkk-leistungen/spezielle-behandlungsangebote/movival-aktiv-gegen-krebs)|1,000+|/|/|/|/|/|/|
|HUK|[Meine Gesundheit](https://www.huk.de/service/apps.html)|50,000+|/|/|/|/|/|/|
|HUK|[PKV-Rechnungs-App](https://www.huk.de/service/apps.html)|50,000+|/|/|/|/|/|/|
|IKK|[Meine IKK](https://www.ikk-classic.de/pk/sp/apps/ikk-classic)|50,00+|/|/|/|/|/|/|
|IKK|[Muntermacher](https://www.ikk-gesundplus.de/service/ikk_muntermacher/muntermacher_app/)|500+|/|/|/|/|/|/|
|Versicherungskammer Bayern|[Meine Gesundheit](https://www.meine-gesundheit.services/vkb/?module=invoice#/start/)|5,000+|/|/|/|/|/|/|
|SBK|[Meine SBK](https://www.sbk.org/meine-sbk/sbk-leben-1/2019/meine-sbk-als-app/)|100,000+|/|/|/|/|/|/|
|TK|[DiabtesTagebuch](https://www.tk.de/techniker/gesundheit-und-medizin/behandlungen-und-medizin/diabetes/tk-diabetestagebuch-2015770)|10,000+|/|/|/|/|/|/|
|TK|[ICD-Diagno­se­aus­kunft](https://www.tk.de/techniker/magazin/digitale-gesundheit/apps/icd-diagnoseauskunft-app-2025510)|100,000+|/|/|/|/|/|/|
|TK|[TK App](https://www.tk.de/techniker/unternehmensseiten/unternehmen/die-tk-app-2027886)|500,000+|/|/|/|/|/|/|
|uniVersa|[RechnungsApp](https://www.universa.de/kundenservice/serviceleistungen/rechnungsapp-und-e-abrechnung/rechnungsapp-und-e-abrechnung.htm)|10,000+|/|/|/|/|/|/|
|VIACTIV|[VIACTIV Krankenkasse](https://www.viactiv.de/services/online-services-2/viactiv-app)|10,00+|/|/|/|/|/|/|
|Universal|[Ada Health](https://ada.com/de/)|5,000,000+|2.48.0|Nein|[Link](https://www.kuketz-blog.de/ada-gesundheits-app-mit-facebook-tracker/)|[7](https://reports.exodus-privacy.eu.org/en/reports/97766/)|[c't](https://www.heise.de/ct/artikel/Massive-Datenschutzmaengel-in-der-Gesundheits-App-Ada-4549354.html)|[Spiegel](https://www.spiegel.de/netzwelt/apps/ada-app-soll-patientendaten-an-us-unternehmen-verschickt-haben-a-1291157.html) / [Golem](https://www.golem.de/news/datenschutz-gesundheitsapp-ada-uebermittelte-persoenliche-daten-an-tracker-1910-144395.html) / [Ada](https://ada.com/de/editorial/information-shared-with-ada-is-confidential/)|
|Universal|[Alarm Medikamenten-Einnahme](https://www.medisafe.com/)|1,000,000+|8.69.08454|Nein|[Link](https://www.kuketz-blog.de/gesundheits-app-alarm-medikamenten-einnahme-tschuess-privatsphaere/)|[12](https://reports.exodus-privacy.eu.org/en/reports/98192/)|/|/|
|Universal|[Cara Care](https://cara.care/)|10,000+|/|/|/|/|/|/|
|Universal|[CardioSecur](https://www.cardiosecur.com/)|1,000+|/|/|/|/|/|/|
|Universal|[medi companion](https://www.medi.de/produkte/highlights/medi-companion/)|5,000+|/|/|/|/|/|/|
|Universal|[Stress Guide](https://www.kenkou.de/)|5,000+|/|/|/|/|/|/|
|Universal|[Migräne App](https://schmerzklinik.de/2016/10/01/die-migraene-app/)|50,000+|/|/|/|/|/|/|
|Universal|[Mobile Retter](http://www.mobile-retter.de/)|10,000+|/|/|/|/|/|/|
|Universal|[Nichtraucher Helden](https://www.nichtraucherhelden.de/)|5,000+|/|/|/|/|/|/|
|Universal|[Pelvina](https://pelvina.de/)|10,000+|/|/|/|/|/|/|
|Universal|[Selfapy](https://www.selfapy.de/)|5,000+|/|/|/|/|/|/|
|Universal|[teleclinic](https://www.teleclinic.com//)|10,000+|/|/|/|/|/|/|
|Universal|[Tinnitracks](https://www.tinnitracks.com)|10,000+|/|/|/|/|/|/|
|Universal|[Vivy](https://www.vivy.com/)|100,000+|1.16|Nein|[Link](https://www.kuketz-blog.de/gesundheits-app-vivy-datenschutz-bruchlandung/)|[3](https://reports.exodus-privacy.eu.org/en/reports/98165/)<sup>1</sup> |/|[Spiegel](https://www.spiegel.de/netzwelt/apps/vivy-sicherheitsexperte-kritisiert-von-krankenkassen-angebotene-gesundheits-app-a-1228749.html) / [Heise](https://www.heise.de/newsticker/meldung/Datenschutzdebatte-um-neue-Gesundheits-App-Vivy-4169288.html)|

-----
<sup>1</sup> Das Exodus Ergebnis ist für eine andere App-Version!
